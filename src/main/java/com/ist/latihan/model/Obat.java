package com.ist.latihan.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "nm_obat")
@Data
public class Obat {
    @Id
    private Long id;
    private String namaObat;
    private String permissionDesc;

    @OneToMany(mappedBy = "obat")
    private List<Permission> permissions;
}
