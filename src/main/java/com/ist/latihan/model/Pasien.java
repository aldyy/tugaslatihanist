package com.ist.latihan.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "data_pasien")
public class Pasien {

    @Id
    private Long id;
    private String nama;
    private String penyakit;


    @OneToOne
    @JoinColumn(name = "role_id")
    private Obat obat;

}
