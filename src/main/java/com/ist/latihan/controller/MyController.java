package com.ist.latihan.controller;

import com.ist.latihan.model.Pasien;
import com.ist.latihan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MyController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/pasien")
    public List<Pasien> getAllPasien() {
        return userRepository.findAll();
    }

    @PostMapping("/savePasien")
    public ResponseEntity<Pasien> savePasien(@RequestBody Pasien pasien) {
        Pasien p = userRepository.save(pasien);
        return new ResponseEntity<>(p, HttpStatus.CREATED);
    }

    @DeleteMapping("/deletePasien")
    public ResponseEntity<String> deletePasien(@RequestParam String userId) {
        userRepository.deleteById(Long.valueOf(userId));
        return new ResponseEntity<>("Delete", HttpStatus.OK);
    }

//    @RequestMapping("/pasien/{id}")
//    public Pasien getPasien(@PathVariable String id) {
//        return pasienService.getPasien(id);
//    }
//
//    @RequestMapping(method = RequestMethod.POST, value = "/pasien")
//    public void addPasien(@RequestBody Pasien pasien){
//        pasienService.addPasien(pasien);
//    }
//
//    @RequestMapping(method = RequestMethod.PUT, value = "/pasien/{id}")
//    public void updatePasien(@RequestBody Pasien pasien, @PathVariable String id){
//        pasienService.updatePasien(id, pasien);
//    }
//
//    @RequestMapping(method = RequestMethod.DELETE, value ="/pasien/{id}")
//    public void deletePasien(@PathVariable String id) {
//        pasienService.deletePasien(id);
//    }

}
