package com.ist.latihan.repository;

import com.ist.latihan.model.Pasien;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Pasien, Long> {
}
