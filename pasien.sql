/*
 Navicat Premium Data Transfer

 Source Server         : coba
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : pasien

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 01/12/2021 09:25:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for data_pasien
-- ----------------------------
DROP TABLE IF EXISTS `data_pasien`;
CREATE TABLE `data_pasien`  (
  `id` int NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `penyakit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci MAX_ROWS = 5 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_pasien
-- ----------------------------
INSERT INTO `data_pasien` VALUES (101, 'banu', 'pilek', 1);
INSERT INTO `data_pasien` VALUES (128, 'baguss', 'batuk', 2);
INSERT INTO `data_pasien` VALUES (129, 'ina', 'batuk', 2);

-- ----------------------------
-- Table structure for nm_obat
-- ----------------------------
DROP TABLE IF EXISTS `nm_obat`;
CREATE TABLE `nm_obat`  (
  `id` int NOT NULL,
  `nama_obat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `permission_desc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci MAX_ROWS = 5 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nm_obat
-- ----------------------------
INSERT INTO `nm_obat` VALUES (1, 'Bodrexin', 'Tersimpan');
INSERT INTO `nm_obat` VALUES (2, 'OBH', 'Tersimpan');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int NOT NULL,
  `role_id` int NULL DEFAULT NULL,
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci MAX_ROWS = 5 ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, 1, '/data/obat/pilek');
INSERT INTO `permission` VALUES (2, 2, '/data/obat/batuk');

SET FOREIGN_KEY_CHECKS = 1;
